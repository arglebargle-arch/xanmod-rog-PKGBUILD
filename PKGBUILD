# Maintainer: Arglebargle < arglebargle at arglebargle dot dev>
# Contributor: Joan Figueras <ffigue at gmail dot com>
# Contributor: Torge Matthies <openglfreak at googlemail dot com>
# Contributor: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# --
# shellcheck disable=SC2034,SC2164

## The following variables can be customized at build time
##
##  Usage:  env microarchitecture=98 makepkg -sc
##     or:  makepkg -sc -- microarchitecture=98
##     or:  export microarchitecture=98; makepkg -sc

##
## Xanmod options:
##
## Look inside 'choose-gcc-optimization.sh' to choose your microarchitecture
## Valid numbers between: 0 to 99
## Default is: 93 => x86-64-v3
## Good option if your package is for one machine: 98 (Intel native) or 99 (AMD native)
: "${microarchitecture:=93}"

## Enable -O3 compiler optimizations, may or may not improve performance. Compilation time increases.
## Set variable "O3" to: n to disable (default)
##                       y to enable
: "${O3:=n}"

## Choose between GCC or CLANG config (default is GCC)
case "${compiler,,}" in
  "clang" | "gcc") compiler=${compiler,,} ;;
                *) compiler=gcc            ;; # default to GCC
esac

## Clang/LLVM compilation flags
if [[ "$compiler" == 'clang' ]]; then
  _COMPILER_FLAGS='CC=clang HOSTCC=clang LD=ld.lld LLVM=1 LLVM_IAS=1'
fi

# Compile ONLY used modules to VASTLY reduce the number of modules built
# and the build time.
#
# To keep track of which modules are needed for your specific system/hardware,
# give module_db script a try: https://aur.archlinux.org/packages/modprobed-db
# This PKGBUILD read the database kept if it exists
#
# More at this wiki page ---> https://wiki.archlinux.org/index.php/Modprobed-db
: "${_localmodcfg:=n}"

# Tweak kernel options prior to a build via nconfig
_makenconfig=

# 'makeflags_check' - If MAKEFLAGS in /etc/makepkg.conf is null or unset we'll throw a big
#                     warning and pause long enough for the user to cancel their build.
#                     Set "makeflags_check" to n to skip this
#
: "${makeflags_check:=y}"

### IMPORTANT: Do not edit below this line unless you know what you're doing

pkgbase=linux-xanmod-rog
xanmod=6.4.2-xanmod1      # unused at the moment, normally we pull this patch and apply it
#pkgver=${xanmod//-/.}
# track the xanmod version as the last .segment here; this is used as part of the
# compiled kernel's localversion later
pkgver=6.4.4.xanmod1
pkgrel=2
pkgdesc='Linux Xanmod ROG'
url="http://www.xanmod.org/"
arch=(x86_64)
license=(GPL2)
makedepends=(
  bc
  cpio
  # the x86-64-vY targets require gcc >= 11.0
  "gcc>=11.0"
  kmod
  libelf
  pahole
  python
  perl
  tar
  xz

  # htmldocs; we don't actually build these
  #graphviz
  #imagemagick
  #python-sphinx
  #texlive-latexextra
)
if [ "$compiler" = "clang" ]; then
  pkgver="${pkgver}+clang"
  makedepends+=(clang llvm lld)
  _LLVM=1
fi
options=('!strip' '!ccache')
_major=${xanmod%\.*\-*}           # eg: 5.15
_branch=${xanmod%%\.*\-*}.x       # eg: 5.x

source=("https://cdn.kernel.org/pub/linux/kernel/v${_branch}/linux-${_major}.tar."{xz,sign}
        #"https://github.com/xanmod/linux/releases/download/${xanmod}/patch-${xanmod}.xz"
        "choose-gcc-optimization.sh"

        # apply any incremental stable kernel updates ahead of official Xanmod release
        #"https://cdn.kernel.org/pub/linux/kernel/v5.x/incr/patch-5.3.5-6.xz"
        #"https://cdn.kernel.org/pub/linux/kernel/v6.x/patch-6.3.7.xz"
        
        # We'll handle patching against Xanmod a little differently for this release;
        # it's getting to be quite a bit of :effort: to selectively revert changes in
        # the Xanmod tree so we'll build against our own patch on top of kernel v6.3
        "Linux-6.4.4-xanmod-rog1.patch.xz"

        # Arch: any misc hotfixes

        # tcp_bbr2 included in Xanmod
        # various sched/fairq scheduler improvements included in Xanmod

        # -- patch from Chromium developers; more accurately report battery state changes
        "acpi-battery-Always-read-fresh-battery-state-on-update.patch"

        # -- ASUS ROG patches from fluke Jones' linux-rog fedora kernel

        # GU604 Realtek audio patch -- merged in 6.3.7
        # X13 tablet mode switch support
        "0001-HID-amd_sfh-Add-support-for-tablet-mode-switch-senso.patch"

        # IRQ override FA507NV
        "0001-ACPI-resource-Skip-IRQ-override-on-ASUS-TUF-Gaming-A.patch"
        # IRQ override FA617NS
        "0002-ACPI-resource-Skip-IRQ-override-on-ASUS-TUF-Gaming-A.patch"

        # fix TUF A16 keyboard
        "0001-pinctrl-amd-Remove-incorrect-reference-to-PULL_UP_SE.patch"
        "v3_20230601_mario_limonciello_acpi_resource_remove_zen_specific_match_and_quirks.mbx"

        # (v6.5) Z13 keyboard support
        "0003-HID-asus-Add-support-for-ASUS-ROG-Z13-keyboard.patch"
        # (v6.5) handle unhandled keycodes on some keyboard models
        "0004-HID-asus-add-keycodes-for-0x6a-0x4b-and-0xc7.patch"
        # (v6.5) housekeeping
        "0005-HID-asus-reformat-the-hotkey-mapping-block.patch"

        # add support for ASUS screenpad laptops
        "v4-0001-platform-x86-asus-wmi-add-support-for-ASUS-screen.patch"

        # audio support
        "v2_20230704_luke_alsa_hda_realtek_add_extra_roog_laptop_quirks.mbx"

        # various wmi patches to improve device support
        "v2_20230630_luke_platform_x86_asus_wmi.mbx"
        "v3_20230720_kristiana2000_platform_x86_asus_wmi_fix_setting_rgb_mode_on_some_tuf_laptops.mbx"

        # mediatek mt79xx bt/wifi

        )
validpgpkeys=(
    'ABAF11C65A2970B130ABE3C479BE3E4300411886' # Linux Torvalds
    '647F28654894E3BD457199BE38DBBDC86092693E' # Greg Kroah-Hartman
)

sha256sums=('8fa0588f0c2ceca44cac77a0e39ba48c9f00a6b9dc69761c02a5d3efac8da7f3'
            'SKIP'
            '1e555a4ea6173dc229d3a1ebe852863eef64d33a8bf4858b91357828ab7122c4'
            '028785ab889865dfbd08f0304be34458be4c402b19bf1ebdbc744ade303a4606'
            'f7a4bf6293912bfc4a20743e58a5a266be8c4dbe3c1862d196d3a3b45f2f7c90'
            'ecb58d154ef6907362280fca8451c998dc000b0a700e2b9848bbe54dc84b560d'
            'a00b952d53df9d3617d93e8fba4146a4d6169ebe79f029b3a55cca68f738d8ea'
            '4912b1319e46ddd6670147f5e878b4aca8bcfbd7b5c852fe11e434e424666365'
            'bf29e9751c05999d3f8210ccbc855b0877c810d80ae1e1443be5b4dc39979d84'
            'd3e4e8492c9d7f84b9dd53daef2ab56cda4363154126fa7cd94ff95bb70fd49d'
            '655a7650a21ce4f725caf6fa248295edefa25a248aeaecf3d65a058503ae2530'
            '7ce4b001a81b15b5b5275620fc0cee3d251d753698ae3db4593619a2121e1f2e'
            'c7d44e1eb82b4711b4cc86016a1886a573f1abc893dbdd201d4a6d0326859b85'
            '2e0274f6681d22f0350bb04cab4bbe796e97c9bb1bd297054eaf900046036d81'
            'ea29d7f1b1eb28f9a0a8838353ce611c0186aaf311861ac5fc36d21a1b24601e'
            'fc7cf7fa948800b4b08b62dc0ffaef7cf94d49a8d2273a1d517d3d1d96207dcd'
            '75c9196d01e14a3cdb4af18120325e24675936d29964a05084b673196b6853e6')

export KBUILD_BUILD_HOST=${KBUILD_BUILD_HOST:-archlinux}
export KBUILD_BUILD_USER=${KBUILD_BUILD_USER:-"$pkgbase"}
export KBUILD_BUILD_TIMESTAMP=${KBUILD_BUILD_TIMESTAMP:-$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})}

# shellcheck disable=SC2154,SC2155
prepare() {
  cd "linux-${_major}"

  # Apply patches
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"

    # handle any of our files that have been decompressed in passing by makepkg
    # trim decompressed extensions to simplify matching below
    case "$src" in
      *.zstd) src=${src%\.zstd} ;;
      *.zst) src=${src%\.zst} ;;
      *.xz) src=${src%\.xz} ;;
    esac

    case "$src" in
      patch-${_major}*xanmod*|Linux-${_major}*xanmod*)
        # Apply Xanmod patch
        msg2 "Applying Xanmod patch ${src} ..."
        patch -Np1 -i "../${src}"
        ;;
      patch-${_major}*|Linux-${_major}*)
        # Apply any kernel.org point release if we're building ahead of Xanmod upstream
        msg2 "Applying kernel.org point release ${src} ..."
        patch -Np1 -i "../${src}"
        ;;
      *.patch|*.diff|*.mbx)
        # Apply any other patches
        msg2 "Applying patch ${src} ..."
        patch -Np1 -i "../${src}"
        ;;
    esac
  done

  # Set kernel version
  msg2 "Setting version..."
  echo "-$pkgrel" > localversion.99-pkgrel
  #msg "localversion.99-pkgrel = $(cat localversion.99-pkgrel)"
  echo "${pkgbase#linux-xanmod}" > localversion.20-pkgname
  #msg "localversion.20-pkgname = $(cat localversion.20-pkgname)"

  # Extract localversions and update source if needed
  local _xanlver=${xanmod##*\-}   # strip longest match '^*\-'
  #msg "$(declare -p _xanlver)"
  local _pkglver=${pkgver##*\.}   # strip longest match '^*\.'
  #msg "$(declare -p _pkglver)"
  _pkglver=${_pkglver%\+*}        # strip shortest match '\+*$' (removes +clang tag)
  [[ "$_xanlver" != "$_pkglver" ]] &&
    sed -Ei "s/xanmod[0-9]+/${_pkglver}/" localversion
  #msg "localversion = $(cat localversion)"

  # Apply configuration; note that it doesn't matter which xanmod -v? config you pick here since
  # we're about to run choose-gcc-optimization shortly anyway, lol
  msg2 "Applying kernel config..."
  cp -vf CONFIGS/xanmod/gcc/config_x86-64-v1 .config

  # enable LTO_CLANG_THIN
  if [ "$compiler" = "clang" ]; then
    msg2 "Enabling Clang ThinLTO ..."
    scripts/config --disable LTO_CLANG_FULL \
                   --enable LTO_CLANG_THIN
  fi

  # enable O3 optimizations
  if [ "$O3" = "y" ]; then
    msg2 "Enabling -O3 optimizations ..."
    scripts/config --disable CONFIG_CC_OPTIMIZE_FOR_PERFORMANCE \
                   --enable CONFIG_CC_OPTIMIZE_FOR_PERFORMANCE_O3
  fi

  # CONFIG_STACK_VALIDATION gives better stack traces. Also is enabled in all official kernel packages by Archlinux team
  scripts/config --enable CONFIG_STACK_VALIDATION

  # Enable IKCONFIG following Arch's philosophy
  scripts/config --enable CONFIG_IKCONFIG \
                 --enable CONFIG_IKCONFIG_PROC

  # Compress modules
  scripts/config --disable CONFIG_MODULE_COMPRESS_NONE \
                 --enable CONFIG_MODULE_COMPRESS_ZSTD

  # Select microarchitecture optimization target
  "${srcdir}/choose-gcc-optimization.sh" "$microarchitecture"

  # Apply package config customizations
  if [[ -s ${startdir}/xanmod-rog-config ]]; then
    msg2 "Applying package config customization..."
    bash -x "${startdir}/xanmod-rog-config"
  fi

  # This is intended for the people that want to build this package with their own config
  # Put the file "myconfig" at the package folder (this will take precedence) or "${XDG_CONFIG_HOME}/linux-xanmod-rog/myconfig"
  # If we detect partial file with scripts/config commands, we execute as a script
  # If not, it's a full config, will be replaced
  for _myconfig in "${startdir}/myconfig" "${HOME}/.config/linux-xanmod-rog/myconfig" "${XDG_CONFIG_HOME}/linux-xanmod-rog/myconfig" ; do
    # if file exists and size > 0 bytes
    if [ -s "${_myconfig}" ]; then
      if grep -q 'scripts/config' "${_myconfig}"; then
        # myconfig is a partial file. Executing as a script
        msg2 "Applying myconfig..."
        bash -x "${_myconfig}"
      else
        # myconfig is a full config file. Replacing default .config
        msg2 "Using user CUSTOM config..."
        cp -f "${_myconfig}" .config
      fi
      echo
      break
    fi
  done

  ### Optionally load needed modules for the make localmodconfig
  # See https://aur.archlinux.org/packages/modprobed-db
  if [ "$_localmodcfg" = "y" ]; then
    if [ -f "$HOME/.config/modprobed.db" ]; then
      msg2 "Running Steven Rostedt's make localmodconfig now"
      make "${_COMPILER_FLAGS}" LSMOD="$HOME/.config/modprobed.db" localmodconfig
    else
      msg2 "No modprobed.db data found"
      exit 1
    fi
  fi

  msg2 "Finalizing kernel config..."
  make "${_COMPILER_FLAGS}" olddefconfig

  make -s kernelrelease > version
  msg2 "Prepared %s version %s" "$pkgbase" "$(<version)"

  [[ -z "$_makenconfig" ]] || make "${_COMPILER_FLAGS}" nconfig

  # save build configuration for reuse or inspection
  cat .config > "${startdir}/config.last"
}

build() {

  if [[ -z "$MAKEFLAGS" ]] && [[ "$makeflags_check" == "y" ]]; then
    show_makeflags_warning
  fi

  cd "linux-${_major}"
  make "${_COMPILER_FLAGS}" all
}

# shellcheck disable=SC2154,SC2155
_package() {
  pkgdesc="A curated $pkgdesc kernel (and modules) with improved support for ASUS laptops"
  depends=(coreutils kmod initramfs)
  optdepends=('crda: to set the correct wireless channels of your country'
              'linux-firmware: firmware images needed for some devices')
  provides+=(VIRTUALBOX-GUEST-MODULES
             WIREGUARD-MODULE
             KSMBD-MODULE
             NTFS3-MODULE
             ASUS-WMI-FAN-CONTROL
             linux-rog)

  cd "linux-${_major}"
  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  msg2 "Installing boot image..."
  # systemd expects to find the kernel here to allow hibernation
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  install -Dm644 "$(make -s image_name)" "$modulesdir/vmlinuz"

  # Used by mkinitcpio to name the kernel
  echo "$pkgbase" | install -Dm644 /dev/stdin "$modulesdir/pkgbase"

  msg2 "Installing modules..."
  make INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 modules_install

  # remove build and source links
  rm "$modulesdir"/{source,build}
}

# shellcheck disable=SC2154,SC2155
_package-headers() {
  pkgdesc="Headers and scripts for building modules for the $pkgdesc kernel"
  depends=(pahole)

  cd "linux-${_major}"
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  msg2 "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map \
    localversion.* version vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile
  cp -t "$builddir" -a scripts

  # required when STACK_VALIDATION is enabled
  install -Dt "$builddir/tools/objtool" tools/objtool/objtool

  # required when DEBUG_INFO_BTF_MODULES is enabled
  [[ -x tools/bpf/resolve_btfids/resolve_btfids ]] &&
    install -Dt "$builddir/tools/bpf/resolve_btfids" tools/bpf/resolve_btfids/resolve_btfids

  msg2 "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/x86" -a arch/x86/include
  install -Dt "$builddir/arch/x86/kernel" -m644 arch/x86/kernel/asm-offsets.s

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  msg2 "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  msg2 "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */x86/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  msg2 "Removing documentation..."
  rm -r "$builddir/Documentation"

  msg2 "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  msg2 "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  msg2 "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v "$STRIP_SHARED" "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v "$STRIP_STATIC" "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v "$STRIP_BINARIES" "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v "$STRIP_SHARED" "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  msg2 "Stripping vmlinux..."
  strip -v "$STRIP_STATIC" "$builddir/vmlinux"
  msg2 "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}

pkgname=("${pkgbase}" "${pkgbase}-headers")
for _p in "${pkgname[@]}"; do
  eval "package_$_p() {
    $(declare -f "_package${_p#"$pkgbase"}")
    _package${_p#"$pkgbase"}
  }"
done

# shellcheck disable=SC2059
show_makeflags_warning() {
  echo
  warning "makepkg.conf MAKEFLAGS is not set, your kernel build may take many hours to complete!"
  plainerr ""
  plainerr "See https://wiki.archlinux.org/title/Makepkg#Improving_compile_times for help"
  plainerr "or scan this handy-dandy QR code to go straight to the wiki article:"
  plainerr ""
  if [[ "$(/bin/date +%m%d)" == "0401" ]]; then
    # Happy April 1!
    cat <<-YEET >&2
     ▄▄▄▄▄▄▄ ▄▄    ▄ ▄▄▄▄▄ ▄▄▄▄▄▄▄  
     █ ▄▄▄ █ ▄  ▄▄█▄  ▀█ ▄ █ ▄▄▄ █  
     █ ███ █ ██▄█ █ █▀▀▀█  █ ███ █  
     █▄▄▄▄▄█ ▄▀▄ █▀▄ ▄▀█▀█ █▄▄▄▄▄█  
     ▄▄▄▄  ▄ ▄▀ ▀ ▄▄▀▀███▀▄  ▄▄▄ ▄  
     ▄▄█▄█▀▄▀▄▀   ▄▀ █ ▄▀█ ███ ▄▄▀  
      █▄█▀▄▄▀ ▄ █▀██▄█▄▀▄▀▀▀▀▀▄▄ ▀  
     █▀▄▀██▄ ▀▄█▀▄ █ █▀ ██▄▀█▄ ███  
     █▀▄██ ▄ ▀ ▄▄▀ ▀▀▀ ▄ █▄▀▀█▄ █   
     ▄▀▀▄▀ ▄▀██▄▄█ ▀█▄ ▀ ▀▀ █ ▀█▀   
      ▄▀█▀▀▄▄▄▄▄▄█ █▄▀█▄███▄▄▄▄█    
     ▄▄▄▄▄▄▄ ▀██▄█▄▄   ▀▄█ ▄ ██▀█▀  
     █ ▄▄▄ █  ▀▄ ▄▀██▄▄▀ █▄▄▄█▀▄█▄  
     █ ███ █ █ ▄█▀▄ ▀▀  ▀▀█ ▄▀▀▄ █  
     █▄▄▄▄▄█ █  ▀  █▄█ ▀██  ▀ █ █
YEET
  else
    cat <<-YEET >&2
     ▄▄▄▄▄▄▄  ▄    ▄ ▄  ▄▄▄▄▄  ▄▄▄▄▄▄▄
     █ ▄▄▄ █ ▀█▄█▀ ▀▀█ █ ██▀▄  █ ▄▄▄ █
     █ ███ █ ▀▀▄▀▄█▀▄█▀ █▄ ▀██ █ ███ █
     █▄▄▄▄▄█ █ █ ▄ ▄▀█▀█ █▀▄ ▄ █▄▄▄▄▄█
     ▄▄▄▄▄ ▄▄▄▄▄ █▄██▀▄▀▄ ▀█▀▄▄ ▄ ▄ ▄ 
     █ ██▀ ▄▄▀█ ▄▄▀▄▄▀█▀ ███▀▄ ▀▄▄▄▀█▀
     ▄▀▄█▀▀▄ ▀█▀█ ▄█▀▀▄ ▀ ▀▄▀▀█▀▀▄▄▀  
     █ ▀▀▀█▄▄█▄█ ▀ ▄▄█▄ ▀▄▄▄▀  █    █▀
     ▀█▀▀██▄ ▀█  █▄█▀ ▄▀▀▀▀▄█▄▄ █▄ ▀▄ 
     ▄▀▀ ▀█▄▀▄▀ ▄▄▄▀▄▀▀█▀██▄▀▄▄█▄▄█ █▀
     ▀ ▄▄▀ ▄█▄█ █  ▀█▄▄ █ ▀▀█▄▄ ▄█ ▀▄ 
     █▀▀▀█▄▄▄▄▄▀ ▀█▄▄██▀▀▀ ▄▀█ █▀ █ █▀
     █ ▀ ▀▀▄█▀▄  █▀▄▀▀▄▀ ▄██▄██▄███▀ ▄
     ▄▄▄▄▄▄▄ █▄▄▄▄  ▄█▄▀▀█▄▄██ ▄ ██▀▄▀
     █ ▄▄▄ █ ▄███  ▀ ▀   ▀▀█▀█▄▄▄█▀▀  
     █ ███ █ █▀▄ ▀██▄██ ▀██ ██▀▄▀▀▄▄▄ 
     █▄▄▄▄▄█ █▄▀ █▀▄▄ ▄ █ ▀██▄▀█▄█▀▀▄ 
YEET
  fi
  plainerr ""
  plainerr "Edit your makepkg.conf and set appropriate MAKEFLAGS for your system to remove this warning"
  plainerr "or run makepkg with 'env makeflags_check=n makepkg ...'"
  plainerr ""
  printf -- "${ALL_OFF}${BOLD} > ${YELLOW}Pressing CTRL-C now and fixing this before continuing is a very good idea ${ALL_OFF}${BOLD}<\n" >&2
  plainerr ""
  printf -- "-> Continuing in 60s " >&2
  for i in {1..60}; do printf "." >&2 && sleep 1; done
  echo
}

# vim:set ts=8 sts=2 sw=2 et:
